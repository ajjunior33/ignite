const express = require('express');

const app = express();

/*
    ## Metodos
    * GET         - Buscar um ifnormação dentro do servidor
    * POST        - Inserir um informação no servidor
    * PUT         - Alterar uma informação no servidor
    * PATCH       - Alterar uma informação no servidor
    * DELETE      - Deltar um informação no servidor
*/
/*
    ## Tipos de parâametros
    * Rotue Params         - Identificar um recurso editar/deletar/buscar.
    * Query Params         - Paginação / Filtros.
    * Body Params          - Os objectos incerção / alteração 
    * 
*/
app.use(express.json());
app.get('/courses', (request,response) => {
    const query = request.query;
    console.log(query)
    return response.json(['Curso 1', 'Curso 2', 'Curso 3']);
});
app.post('/courses', (request,response) => {
    const body = request.body;
    console.log(body)
    return response.json(['Curso 1', 'Curso 2', 'Curso 3', 'Curso 4']);
});
app.put('/courses/:id', (request,response) => {
    // const params = request.params;
    // console.log(params);

    const { id } =  request.params;
    console.log(id)
    return response.json(['Curso 6', 'Curso 2', 'Curso 3', 'Curso 4']);
});

app.patch('/courses/:id', (request,response) => {
    return response.json(['Curso 6', 'Curso 7', 'Curso 3', 'Curso 4']);
});

app.delete('/courses/:id', (request,response) => {
    return response.json(['Curso 6', 'Curso 2', 'Curso 4']);
});

app.listen(3333, () => {
    console.log("Server Running");
})