/**
 * name: strig;
 * ducration: number
 * educator: string
*/
interface Course {
    name: string; 
    ducration?: number; 
    educator: string;
}

class CreateCourseService {
    execute({name, ducration = 8 , educator}: Course) {
        console.log(name, ducration, educator)
    }
}

export default new CreateCourseService;